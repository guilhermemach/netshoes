export class Product {

     constructor(
      public id: number,
      public sku: string,
      public title:string,
      public description:string,
      public availableSizes:string[],
      public style:string,
      public price: any,
      public installments: number,
      public currencyId: string,
      public currencyFormat: string,
      public isFreeShipping: boolean,
      public quantity: number,
      public size: string
      
      ){}
}