"use strict";
var Cart = (function () {
    function Cart(products, total) {
        this.products = products;
        this.total = total;
    }
    return Cart;
}());
exports.Cart = Cart;
//# sourceMappingURL=Cart.js.map