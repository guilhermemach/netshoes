"use strict";
var Product = (function () {
    function Product(id, sku, title, description, availableSizes, style, price, installments, currencyId, currencyFormat, isFreeShipping, quantity, size) {
        this.id = id;
        this.sku = sku;
        this.title = title;
        this.description = description;
        this.availableSizes = availableSizes;
        this.style = style;
        this.price = price;
        this.installments = installments;
        this.currencyId = currencyId;
        this.currencyFormat = currencyFormat;
        this.isFreeShipping = isFreeShipping;
        this.quantity = quantity;
        this.size = size;
    }
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=Product.js.map