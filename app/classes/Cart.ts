import { Product } from './Product';
export class Cart {

     constructor(
      public products: Product [],
      public total: number,          
      ){}
}