import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  moduleId: module.id,
  templateUrl: '/app/pages/itens.html',
})
export class ItensComponent implements OnInit {
  total: number = 0;

  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    console.log("ItensComponent > constructor  begin"); 
      this.route.params.forEach((params: Params) => {
      this.total = params['id'];
      console.log(this.total)
    });
    console.log("ItensComponent > constructor  end");
  }
}