"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var cart_service_1 = require('../services/cart.service');
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var router_2 = require('@angular/router');
var common_1 = require('@angular/common');
var CartComponent = (function () {
    function CartComponent(route, router, location, cartServive) {
        this.route = route;
        this.router = router;
        this.location = location;
        this.cartServive = cartServive;
        this.products = [];
        this.total = 0;
        this.currency = "R$";
    }
    CartComponent.prototype.ngOnInit = function () {
        console.log("CartComponent > ngOnInit  begin");
        this.getAll();
        console.log("CartComponent > ngOnInit  end");
    };
    CartComponent.prototype.remove = function (product) {
        this.cartServive.remove(product);
        this.getAll();
        var total = this.cartServive.size();
        console.log(total);
        this.router.navigate(['/home', { outlets: { 'itens': ['itens', total] } }]);
    };
    CartComponent.prototype.getAll = function () {
        var _this = this;
        this.total = 0;
        this.products = this.cartServive.getAll();
        this.products.forEach(function (prod) { return (_this.total = _this.total + prod.quantity * prod.price); });
    };
    CartComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: '/app/pages/cart.html',
        }), 
        __metadata('design:paramtypes', [router_2.ActivatedRoute, router_1.Router, common_1.Location, cart_service_1.CartService])
    ], CartComponent);
    return CartComponent;
}());
exports.CartComponent = CartComponent;
//# sourceMappingURL=cart.component.js.map