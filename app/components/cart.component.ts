import { GroupProduct } from '../classes/GroupProduct';
import { CartService } from '../services/cart.service';
import { Component, OnInit }      from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';
import { Location }               from '@angular/common';
import { Product } from '../classes/product';
import { ProductService } from '../services/product.service';


@Component({
  moduleId: module.id,
  templateUrl : '/app/pages/cart.html',
})
export class CartComponent implements OnInit {
  products: Product[] = [];
  total: number = 0;
  currency: string = "R$";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private cartServive: CartService
  ) {}

  ngOnInit(): void {    
    console.log("CartComponent > ngOnInit  begin");    
    this.getAll();
    console.log("CartComponent > ngOnInit  end");
  }

  remove(product : Product){
    this.cartServive.remove(product);
    this.getAll();

    let total = this.cartServive.size();
    console.log(total);
    this.router.navigate(['/home', {outlets: {'itens':['itens', total] }}]);
  }
 
  getAll(){
    this.total = 0;
    this.products =  this.cartServive.getAll();    
    this.products.forEach(prod => ( this.total = this.total + prod.quantity * prod.price));
  }
}