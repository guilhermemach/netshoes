import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'index',
  templateUrl : '/app/pages/index.html'
})
export class IndexComponent {
}