import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../classes/product';
import { ProductService } from '../services/product.service';

@Component({
  moduleId: module.id,
  templateUrl : '/app/pages/products.html',
  providers: [ProductService]
})
export class HomeComponent {

  products: Array<Product> = [];
  messageError : string = "";
  count : number = 1;

  constructor(private router: Router, private productService: ProductService) {   
    console.log('HomeComponent started...');    
  }

  ngOnInit(){
    console.log('ngOnInit...');
    this.getAllProducts() ;
  }
  
  getAllProducts() {

    console.log('getAllProducts()... begin');

    this.productService.getAll().subscribe(
                retorno => (this.products = retorno),
                error => this.messageError = <any>error,
    );

    console.log('getAllProducts()... end');
  }

  onSelect(product: Product): void {
     this.router.navigate(['/home', {outlets: {'cart':['product', product.id]}}]);
  }
  
}