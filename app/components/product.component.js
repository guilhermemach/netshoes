"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var cart_service_1 = require('../services/cart.service');
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var router_2 = require('@angular/router');
var common_1 = require('@angular/common');
var product_service_1 = require('../services/product.service');
var ProductComponent = (function () {
    function ProductComponent(route, router, location, productService, cartServive) {
        this.route = route;
        this.router = router;
        this.location = location;
        this.productService = productService;
        this.cartServive = cartServive;
        this.product = null;
    }
    ProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log("ProductComponent > ngOnInit  begin");
        this.route.params.forEach(function (params) {
            _this.id = params['id'];
            _this.productService.get(_this.id).subscribe(function (retorno) { return (_this.product = retorno, console.log(retorno.title)); });
        });
        console.log("ProductComponent > ngOnInit  end");
    };
    ProductComponent.prototype.addCart = function (product) {
        this.cartServive.add(product);
        var total = this.cartServive.size();
        this.router.navigate(['/home', { outlets: { 'cart': ['cart'], 'itens': ['itens', total] } }]);
    };
    ProductComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: '/app/pages/product.html',
        }), 
        __metadata('design:paramtypes', [router_2.ActivatedRoute, router_1.Router, common_1.Location, product_service_1.ProductService, cart_service_1.CartService])
    ], ProductComponent);
    return ProductComponent;
}());
exports.ProductComponent = ProductComponent;
//# sourceMappingURL=product.component.js.map