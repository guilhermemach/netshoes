"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var product_service_1 = require('../services/product.service');
var HomeComponent = (function () {
    function HomeComponent(router, productService) {
        this.router = router;
        this.productService = productService;
        this.products = [];
        this.messageError = "";
        this.count = 1;
        console.log('HomeComponent started...');
    }
    HomeComponent.prototype.ngOnInit = function () {
        console.log('ngOnInit...');
        this.getAllProducts();
    };
    HomeComponent.prototype.getAllProducts = function () {
        var _this = this;
        console.log('getAllProducts()... begin');
        this.productService.getAll().subscribe(function (retorno) { return (_this.products = retorno); }, function (error) { return _this.messageError = error; });
        console.log('getAllProducts()... end');
    };
    HomeComponent.prototype.onSelect = function (product) {
        this.router.navigate(['/home', { outlets: { 'cart': ['product', product.id] } }]);
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: '/app/pages/products.html',
            providers: [product_service_1.ProductService]
        }), 
        __metadata('design:paramtypes', [router_1.Router, product_service_1.ProductService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map