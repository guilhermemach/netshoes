import { CartService } from '../services/cart.service';
import { Component, OnInit }      from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';
import { Location }               from '@angular/common';
import { Product } from '../classes/product';
import { ProductService } from '../services/product.service';


@Component({
  moduleId: module.id,
  templateUrl : '/app/pages/product.html',
})
export class ProductComponent implements OnInit {
  product: Product = null;
  id: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private productService: ProductService,
    private cartServive: CartService
  ) {}

  ngOnInit(): void {
    
    console.log("ProductComponent > ngOnInit  begin");
    this.route.params.forEach((params: Params) => {
      this.id = params['id'];
      this.productService.get(this.id).subscribe(
                retorno => (this.product = retorno, console.log(retorno.title)),                
     );
    });

    console.log("ProductComponent > ngOnInit  end");
  }

  addCart(product : Product): void {
      this.cartServive.add(product);
      let total = this.cartServive.size();
      this.router.navigate(['/home', {outlets: {'cart':['cart'], 'itens':['itens', total] }}]);
  }
}