import { ItensComponent } from './components/itens.component';
import { CartComponent } from './components/cart.component';
import { IndexComponent } from './components/index.component';
import { HomeComponent }   from './components/home.component';
import { ProductComponent }   from './components/product.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const appRoutes: Routes = [{   
    path: 'home',
      component: IndexComponent,
      children: [
        { path: '', component: HomeComponent }  ,    
        { path: 'product/:id', component: ProductComponent, outlet: 'cart'},
        { path: 'cart', component: CartComponent, outlet: 'cart'},
        { path: 'itens/:id', component: ItensComponent, outlet: 'itens'}
      ]
    },
     { 
      path: '', 
      redirectTo: '/home',
      pathMatch: 'full'
   },
   
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes); 