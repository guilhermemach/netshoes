"use strict";
var itens_component_1 = require('./components/itens.component');
var cart_component_1 = require('./components/cart.component');
var index_component_1 = require('./components/index.component');
var home_component_1 = require('./components/home.component');
var product_component_1 = require('./components/product.component');
var router_1 = require('@angular/router');
var appRoutes = [{
        path: 'home',
        component: index_component_1.IndexComponent,
        children: [
            { path: '', component: home_component_1.HomeComponent },
            { path: 'product/:id', component: product_component_1.ProductComponent, outlet: 'cart' },
            { path: 'cart', component: cart_component_1.CartComponent, outlet: 'cart' },
            { path: 'itens/:id', component: itens_component_1.ItensComponent, outlet: 'itens' }
        ]
    },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map