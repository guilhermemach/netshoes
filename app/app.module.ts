import { CartComponent } from './components/cart.component';
import { ItensComponent } from './components/itens.component';
import { ProductComponent } from './components/product.component';
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { HomeComponent }   from './components/home.component';
import { IndexComponent }   from './components/index.component';
import { ProductService }   from './services/product.service';
import { CartService }   from './services/cart.service';
import { routing, appRoutingProviders } from './app.routing';


@NgModule({
  imports: [
    BrowserModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    HomeComponent,
    ProductComponent,
    CartComponent,
    IndexComponent,
    ItensComponent
  ],
  providers: [
    appRoutingProviders , ProductService, CartService
  ],
  bootstrap: [ IndexComponent ]
})
export class AppModule {}
