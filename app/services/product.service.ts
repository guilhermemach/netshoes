import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { Product } from '../classes/Product';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class ProductService{

  jsonString : string;

  constructor(private http : Http){
    console.log("start service --------------- ProductService")
  }

  getAll(): Observable<Product[]>{
    return this.http.get('http://localhost:3030/products')
    .map((response: Response) => (<Product[]>response.json()))
    .catch(this.handleError);
  }

  get(id: string): Observable<Product>{
    return this.http.get('http://localhost:3030/products/'+id)
    .map((response: Response) => (<Product>response.json()))
    .catch(this.handleError);
  }

  private handleError (error: Response | any) {
   // In a real world app, we might use a remote logging infrastructure
   let errMsg: string;
   if (error instanceof Response) {
     const body = error.json() || '';
     const err = body.error || JSON.stringify(body);
     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
   } else {
     errMsg = error.message ? error.message : error.toString();
   }
   console.error(errMsg);
   return Observable.throw(errMsg);
 }

}