import { Product } from '../classes/Product';
import { Injectable } from '@angular/core';

@Injectable()
export class CartService {

    products: Product[] = [];
    total: number = 0;

    constructor() {
        console.log("start service --------------- CartService")
    }

    getAll(): Product[] {
        return this.products;
    }

    size(): number {
        this.total = 0;
        this.products.forEach(product => this.total  =  this.total + (product.quantity * 1));
        return this.total;
    }

    remove(product: Product) {
        this.products.splice(this.products.indexOf(product), 1);
    }

    add(product: Product) {
        this.products.push(product)
    }


}