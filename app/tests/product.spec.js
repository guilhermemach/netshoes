"use strict";
var testing_1 = require('@angular/core/testing');
var product_component_1 = require('../components/product.component');
describe('Component: ProductComponent', function () {
    var component;
    beforeEach(function () {
        var component;
        testing_1.TestBed.configureTestingModule({
            declarations: [product_component_1.ProductComponent],
        });
        var fixture = testing_1.TestBed.createComponent(product_component_1.ProductComponent);
        component = fixture.componentInstance;
    });
    it('should have a defined component', function () {
        expect(component).toBeDefined();
    });
});
//# sourceMappingURL=product.spec.js.map