import {
    TestBed
} from '@angular/core/testing';

import { ProductComponent } from '../components/product.component';

describe('Component: ProductComponent', () => {
    let component: ProductComponent;
    beforeEach(() => {

        let component: ProductComponent;
        TestBed.configureTestingModule({
            declarations: [ProductComponent],
        });

        const fixture = TestBed.createComponent(ProductComponent);
        component = fixture.componentInstance;
    });

    it('should have a defined component', () => {
        expect(component).toBeDefined();
    });
});

    