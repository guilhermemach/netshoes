# README #

# Install node.js - 6.10 version

* website https://nodejs.org/en/ 

* on project folder install packages.($ npm install )

* run project. npm install


# Install JSON Server as a global module:

* ($ npm install -g json-server)

* Create a project folder and add the products.json file

* run the json static server ($ json-server  products.json -p 3030)


# Run the project on desired browser

* http://localhost:3030